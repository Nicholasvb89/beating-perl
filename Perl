#!/usr/local/bin/perl -w

use strict;

while(<>){
    chomp;
    my @fields = split /\|/;
    my $array = pop @fields;
    $array =~ s/[{}]//g;
    my @array = split /,/ $array;
    foreach my $i (0..$array){
        print join('|', @fields, $i, array[$i]), "\n";
    }
}