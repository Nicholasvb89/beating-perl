# README #

So all I am doing is reading in and transposing each line and giving a index for each line transposed.. Xn = 96 and
the file is typically about 1.5M rows per file.. so we are talking about the difference of maybe 3 minutes a file.

Line Data before:

1	2	3	4	{x1,x2,x3,x4,...Xn}

Line Data After:

1	2	3	4	1	x1
1	2	3	4	2	x2
1	2	3	4	3	x3
1	2	3	4	4	x4
1	2	3	4	5   Xn

However at times I usually need to run 10+ files and this is just preprocessesing